//----------------------------------------------------------
// File: strings.c              Author(s): Simon Désaulniers
// Date: 2018-01-30
//----------------------------------------------------------
// Laboratoire #3 du cours INF3135 à l'Université du
// Québec à Montréal à la session d'hiver 2018.
//----------------------------------------------------------

#include <string.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    for (int i = argc-1; i > 0; --i)
        printf("%s", argv[i]);
    printf("\n");
    return 0;
}

/* vim:set et sw=4 ts=4 tw=120: */

