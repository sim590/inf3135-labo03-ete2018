#include <stdlib.h>
#include <stdio.h>

const double* find_in_array(const double array[], unsigned int size, double element) {
    for (size_t i = 0; i < size; ++i) {
        if (array[i] == element) {
            return &array[i];
        }
    }
    return NULL;
}

int main() {
    const double my_integers[] = { 1.5, 3.2, 4.0, -5.0 };
    printf("Recherche de 5.0 dans {1.5, 3.2, 4.0, -5.0}\n");
    printf("%s\n", find_in_array(my_integers, 4, 5.0) ? "Oui" : "Non");
    printf("Recherche de 1.5 dans {1.5, 3.2, 4.0, -5.0}\n");
    printf("%s\n", find_in_array(my_integers, 4, 1.5) ? "Oui" : "Non");
    return 0;
}
