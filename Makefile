.PHONY: clean
CC     = gcc
CFLAGS = --std=c11 -Wall -Wextra

CFILES  = arrays.c strings.c palindromes.c
OBJECTS = $(CFILES:.c=.o)
EXECS   = arrays strings palindromes

all: $(EXECS)

arrays: arrays.o

%: %.o
	$(CC) -o $@ $<

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -f $(OBJECTS)

